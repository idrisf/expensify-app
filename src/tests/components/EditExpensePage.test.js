import React from 'react';
import { shallow } from 'enzyme';
import { EditExpansePage } from '../../components/EditExpansePage';
import expenses from '../fixtures/expenses';

let editExpense, removeExpense, history, wrapper;

beforeAll(() => {
    editExpense = jest.fn();
    removeExpense = jest.fn();
    history = { push: jest.fn() };
    wrapper = shallow(
        <EditExpansePage 
            editExpense={editExpense} 
            history={history} 
            removeExpense={removeExpense}
            expense={expenses[0]}
        />
    );
});

test('should render EditExpensePage snapshot', () => {
    expect(wrapper).toMatchSnapshot();
});

test('should handle editExpense', () => {
    wrapper.find('ExpenseForm').prop('onSubmit')(expenses[1]);    
    expect(history.push).toHaveBeenLastCalledWith('/');
    expect(editExpense).toHaveBeenLastCalledWith(expenses[0].id ,expenses[1]);
});

test('should handle removeExpense', () => {
    wrapper.find('button').simulate("click");
    expect(history.push).toHaveBeenLastCalledWith('/');
    expect(removeExpense).toHaveBeenLastCalledWith({ id: expenses[0].id});
});