import React from 'react'
import { shallow } from 'enzyme';
import { ExpenseListFilters } from '../../components/ExpenseListFilters';
import { filters, altFilters} from '../fixtures/filters';
import moment from 'moment';

let setTextFilter, sortByDate, sortByAmount, setStartDate, setEndDate, wrapper;

beforeEach(() => {
    setTextFilter = jest.fn();
    sortByDate = jest.fn();
    sortByAmount = jest.fn();
    setStartDate = jest.fn();
    setEndDate = jest.fn();
    wrapper = shallow(
        <ExpenseListFilters 
            filters={filters}
            setTextFilter={setTextFilter}
            sortByDate={sortByDate}
            sortByAmount={sortByAmount}
            setStartDate={setStartDate}
            setEndDate={setEndDate}
        />
    )
});

test('should render ExpenseListFilter correctly', () => {
    expect(wrapper).toMatchSnapshot();
});

test('should render ExpenseListFilter with alt data correctly', () => {
    wrapper.setProps({
        filters: altFilters
    })
    expect(wrapper).toMatchSnapshot();
});

test('should hanlde text chnages', () => {
    const textInput = "test";
    wrapper.find("input").simulate("change", {
        target : { value: textInput }
    });
    expect(setTextFilter).toHaveBeenLastCalledWith(textInput);
});

test('should sort by date', () => {
    const sortBy = "date";
    wrapper.setProps({
        filters: altFilters
    });
    wrapper.find("select").simulate("change", {
        target : { value: sortBy }
    });
    expect(sortByDate).toHaveBeenCalled();
});

test('should sort by amount', () => {
    const sortBy = "amount";
    wrapper.find("select").simulate("change", {
        target : { value: sortBy }
    });
    expect(sortByAmount).toHaveBeenCalled();
});

test('should handle date changes', () => {
    const startDate = moment();
    const endDate = moment().add("3", "hours");
    wrapper.find("withStyles(DateRangePicker)").prop("onDatesChange")({ 
        startDate, endDate 
    });
    expect(setStartDate).toHaveBeenLastCalledWith(startDate);
    expect(setEndDate).toHaveBeenLastCalledWith(endDate);
});

test('should handle date focus changes', () => {
    const calendarFocused = "startDate";
    wrapper.find("withStyles(DateRangePicker)").prop("onFocusChange")(calendarFocused);
    expect(wrapper.state("calendarFocused")).toEqual(calendarFocused);
});