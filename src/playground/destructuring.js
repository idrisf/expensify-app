// object destructering

// const person = {
//     name: 'Farhan',
//     age: 26,
//     location: {
//         city: 'manchester',
//         temp: 20
//     }
// };

// const { name : firstName = "Random Person", age } = person;
// console.log(`${firstName} is ${age}`);

// const {temp : temperature, city} = person.location;

// if (temperature && city) {
//     console.log(`It's ${temperature} in ${city}.`);
// }

// const book = {
//     title: "Ego is the Enemy",
//     author: "Ryan Holiday",
//     publisher: {
//         name: "Penguin"
//     }
// }

// const {name : publisherName = "Self-Published"} = book.publisher;
// console.log(publisherName); //Penguin, Self-Published

// Array destructering

const address = [
    "5 hoylake close",
    "Manchester",
    "Greater Manchester",
    "M40 3WU"
];
const [, city, county = "helloWorld"] = address;
console.log(`You are in ${city}, ${county}`);

const item = ["Coffee (hot)", "$2.00", "$2.50", "$2.75"];
const [coffee, , price] = item;
console.log(`A meduim ${coffee} costs ${price}`);