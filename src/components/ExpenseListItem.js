import React from 'react';
import { Link } from 'react-router-dom';

const ExpenseItem = ({id, description, amount, createdAt}) => (
    <div>
        <Link to={`/edit/${id}`} >
            <h3>{description}</h3>
        </Link>
        <p>{description}</p>
        <p>{amount}</p>
        <p>{createdAt}</p>
    </div>
)

export default ExpenseItem