const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require("html-webpack-plugin");
// used to analyse performance
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const ExtractTextPlugin = require('extract-text-webpack-plugin');

//const MiniCssExtractPlugin = require("mini-css-extract-plugin");


const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");

// have to come back to CSS source maps not generating!

const CSSExtract = new ExtractTextPlugin('styles.css');
module.exports = {
    entry: './src/app.js',
    output: {
        path: path.join(__dirname, 'public'),
        filename: 'bundle.js'    
    },
    plugins: [
        new CleanWebpackPlugin(['dist']),
        // new MiniCssExtractPlugin({
        //     // Options similar to the same options in webpackOptions.output
        //     // both options are optional
        //     filename: "styles.css",
        //     chunkFilename: "[id].css"
        //   }),
          new OptimizeCSSAssetsPlugin(),
          CSSExtract
        // new BundleAnalyzerPlugin()
    ],
    module: {
        rules: [{
            loader: 'babel-loader',
            test: /\.js$/,
            exclude: /node_modules/
        }, {
            test: /\.s?css$/,
            use: CSSExtract.extract({
                use: [
                    {
                        loader: 'css-loader',
                        options: {
                          sourceMap: true
                        }
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true
                        }
                    }
                ]
            })
        }, ]
    },
};